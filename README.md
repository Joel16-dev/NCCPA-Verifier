# NCCPA-Verifier

# How to install:

- Make sure python is installed.
- This script requires that you have Google Chrome installed and selenium (python lib) installed.
- If you don't have PIP: look into this (It will help you install PIP) -> https://pip.pypa.io/en/stable/installing/
- Then open CMD/powershell/terminal and use the following command to install selenium using PIP: ```pip install selenium```
- Download the [NCCPA-Verifier-master.zip](https://gitlab.com/Joel16-dev/NCCPA-Verifier/-/archive/master/NCCPA-Verifier-master.zip)
- Open the NCCPA-Verifier-master folder and here open CMD/powershell/terminal.
- Run the script by using the command ```python NCCPA-Verifier.py```.

# How to use:
- Then, select an option 1: Uses an input JSON file with all the user info: (Details on the JSON are below) or option 2: Manually enter fields yourself.
- Hit enter, sit back and let the script do it's job. 
- The desired results will be sent to a well outlined list called 'output.txt'

# JSON format:
The json requires a name and a value like so:
```
	{
		"country": "Some country",
		"first_name": "First name",
		"last_name": "Last name",
		"state": "Some US State"
	},
```

Feel free to look at the input.JSON bundled with the program to see how it works.
