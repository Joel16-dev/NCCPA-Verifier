import json
import os
from selenium import webdriver
from sys import platform
import time


def nccpa_verifier_init():
    if platform == 'linux':
        return 'linux/chromedriver'
    elif platform == 'win32' or platform == 'cygwin':
        return 'windows/chromedriver.exe'
    elif platform == 'darwin':
        return 'mac/chromedriver'


def find_between(s, first, last):
    try:
        start = s.rindex(first) + len(first)
        end = s.rindex(last, start)
        return s[start:end]
    except ValueError:
        return ""


def nccpa_get_menu_option():
    return input('Choose an option (1 = JSON {input.json} | 2 = Manual input entry): ')


def nccpa_get_certifcation():
    # HTML from `<html>`
    browser.execute_script("return document.documentElement.innerHTML;")

    # HTML from `<body>`
    browser.execute_script("return document.body.innerHTML;")

    # HTML from element with some JavaScript
    element = browser.find_element_by_css_selector("div.perimeter")
    browser.execute_script("return arguments[0].innerHTML;", element)

    # HTML from element with `get_attribute`
    element = browser.find_element_by_css_selector("div.perimeter")
    html = element.get_attribute('innerHTML')

    data = find_between(html, 'CertificationMessage">', '</p><p>Thank you')
    return data


def nccpa_verify_json():
    with open("input.json") as json_file:
        json_data = json.load(json_file)

    # Prevent cluttering
    if os.path.exists('output.txt'):
        os.remove('output.txt')

    with open('output.txt', 'a+') as output_file:
        output_file.write('Number of entries: ' + str(len(json_data)) + '\n\n')

        for entry in json_data:
            # browser.find_element_by_id('countryddl').clear()
            country = browser.find_element_by_id('countryddl')
            country.send_keys(entry["country"])

            browser.find_element_by_id('uxFirstName').clear()
            first_name = browser.find_element_by_id('uxFirstName')
            first_name.send_keys(entry["first_name"])

            browser.find_element_by_id('uxLastName').clear()
            last_name = browser.find_element_by_id('uxLastName')
            last_name.send_keys(entry["last_name"])

            # browser.find_element_by_id('stateddl').clear()
            state = browser.find_element_by_id('stateddl')
            state.send_keys(entry["state"])

            verify_button = browser.find_element_by_id('uxVerify')
            verify_button.click()

            data = nccpa_get_certifcation()
            if data:
                output_file.write(entry["first_name"] + ' ' + entry["last_name"] + ', ' + entry["state"] +
                                  ', ' + entry["country"] + '.\n')
                output_file.write(data + '\n\n')

            time.sleep(1)

    return browser


def nccpa_verify_input():
    country = browser.find_element_by_id('countryddl')
    country.send_keys(input('Country: '))

    first_name = browser.find_element_by_id('uxFirstName')
    first_name.send_keys(input('First name: '))

    last_name = browser.find_element_by_id('uxLastName')
    last_name.send_keys(input('Last name: '))

    state = browser.find_element_by_id('stateddl')
    state.send_keys(input('State: '))

    verify_button = browser.find_element_by_id('uxVerify')
    verify_button.click()

    data = nccpa_get_certifcation()
    print(data + '\n')

    return browser


chromedriver = 'drivers/' + nccpa_verifier_init()
os.environ["webdriver.chrome.driver"] = chromedriver

menu_option = nccpa_get_menu_option()

browser = webdriver.Chrome(chromedriver)
browser.get('https://www.nccpa.net/verify-pa')

if menu_option == '1':
    browser = nccpa_verify_json()
elif menu_option == '2':
    browser = nccpa_verify_input()

browser.quit()
